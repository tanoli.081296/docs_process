GT SUPPORT USER GUIDE

# Version 1.0

**Lịch sử thay đổi**:
                                                         
| **Phiên bản** | **Ngày** | **Chi tiết thay đổi** | **Tác giả** | **Kiểm tra nội dung** |
| --- | --- | --- | --- | --- |
| 1.0 | 18-08-2020 | 1st version | UyenTTT | NghiaDT, LocTH2, DatLC, QuanNT3, ManhTT, ThuNT3 |


## I. Mục đích

Tài liệu này nhằm giới thiệu và hướng dẫn sử dụng hệ thống GT Support để
yêu cầu các dịch vụ hỗ trợ mà GT Department (GT Dept.) cung cấp.

## II. Danh sách các dịch vụ hỗ trợ

Trong phiên bản hệ thống GT Support được ra mắt lần này, nhóm phát triển
của GT tập trung vào các yêu cầu dịch vụ cơ bản mà GT Dept. cung cấp.
Những dịch vụ/chức năng khác sẽ được phát triển trong những phiên bản
sau.

Các dịch vụ (Liên quan đến hệ thống) bao gồm:

-   Passport – Hệ thống đăng nhập, đăng ký.

-   Game API – Hệ thống API tích hợp với game.

-   Payment – Hệ thống thanh toán, bán hàng.

-   Promotion – Dịch vụ khuyến mãi.

-   Hệ thống khác – Chưa xác định.

### **1. Passport – Hệ thống đăng nhập, đăng ký.**

Các vấn đề hỗ trợ trong dịch vụ này bao gồm:

| Vấn đề hỗ trợ | Mô tả chi tiết vấn đề hỗ trợ | Thông tin cần người yêu cầu cung cấp/Ví dụ |
| --- | --- | --- |
| **Hỗ trợ vận hành** |
| Cập nhật cấu hình Login Platform | Cập nhật dịch vụ login theo yêu cầu:<br>- Các yêu cầu hỗ trợ sản phẩm thanh tra, xin giấy phép (giới hạn độ tuổi tài khoản Zing ID, bật/ tắt form thông tin…)<br>- Cập nhật cấu hình các kênh đăng nhập.<br>- Cập nhật whitelist user được phép đăng nhập vào sản phẩm... | Ví dụ: Nhờ team hỗ trợ bật lại whitelist và set notify ngày OB cho user theo nội dung như sau: &quot;Welcome to…&quot; <br> - Thời gian hiển thị notify: 10:00 AM ngày 15/08 đến 10:00 AM ngày 18/08/2020
| Chuyển đổi kênh đăng nhập của User | Chuyển đổi kênh đăng nhập (ZingID, Zalo, Google, Facebook, Chơi ngay, …) của User trên Login Platform. **Yêu cầu này cần được duyệt bởi PO/ PM của sản phẩm.** | Thông tin cần cho yêu cầu này gồm:<br>- UID cũ, SocialID cũ, Login type cũ.<br>- UID mới, SocialID mới, Login type mới. <br>Ví dụ: Nhờ Team hỗ trợ chuyển liên kết VIP với thông tin bên dưới: <br>UID cũ : 3333396064419111111  <br>Social ID cũ : 4444442523696011111  <br>UID mới : 5555580581674922222 <br>Social ID mới : 6666648121298222222 |
| Kiểm tra hiện tượng bất thường | Yêu cầu kiểm tra hệ thống khi có hiện tượng bất thường liên quan đến Login. |
| Cấp quyền truy cập tool vận hành Passport | Yêu cầu cấp quyền truy cập vào tool vận hành sản phẩm: <br>- Tool kiểm tra thông tin đăng nhập. <br>- Tool kiểm tra thông tin khách hàng. **Yêu cầu này cần sự phê duyệt từ PM của sản phẩm.** | Thông tin cần cho yêu cầu này gồm:<br>- Domain người cần cấp quyền.<br>- Tool cần cấp quyền. <br>Ví dụ: Nhờ team cấp quyền tool passport giúp em để tra thông tin của nhân vật.<br>Domain: ABC |
| Yêu cầu khác | Các yêu cầu khác về Login. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. | Ví dụ: Nhờ team cung cấp mã mapping Product ID của sản phẩm 424 – Perfect World VNG, để tích hợp lên hệ thống CSTool.|
| **Tích hợp mới** |
| Login Platform cho Game Mobile | Cung cấp dịch vụ login cho sản phẩm mới. | Ví dụ: Trong bản SDK mới của game có kênh login AppleID nên nhờ team cập nhật thêm kênh login AppleID cho webpay của sản phẩm Tân Tiếu Ngạo VNG (TTNGH-447) luôn giúp em.|
| Cung cấp Login cho Web | Cung cấp dịch vụ đăng nhập, đăng ký cho sản phẩm Web/ H5. | Thông tin cần cho yêu cầu này gồm:<br>- Callback URL (đường dẫn để nhận thông tin sau khi user đăng nhập thành công): Phía Passport sẽ cung cấp lại các đường link đăng nhập tương ứng với từng phương thức đăng nhập và các callback URL này.<br>- Ngoài ra Passport có hỗ trợ trang web login phiên bản desktop cho các game mobile và H5, người yêu cầu có thể gửi source html mong muốn hoặc sử dụng template mặc định.<br>Ví dụ: Nhờ team hỗ trợ login Game Samurai Spirit thị trường VN qua tất cả các kênh được hỗ trợ: <br>- GameID  **spvn** <br>- Url Callback: Staging [https://staging.event.zing.vn/samurai-spirit-vn/dang-nhap](https://staging.event.zing.vn/gamecode/dang-nhap)
| Yêu cầu khác | Các yêu cầu khác về tích hợp Login. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. |

### **2. Game API – Hệ thống API tích hợp với game.**

Các vấn đề hỗ trợ trong dịch vụ này bao gồm:

| Vấn đề hỗ trợ | Mô tả chi tiết vấn đề hỗ trợ | Thông tin cần người yêu cầu cung cấp/ Ví dụ |
| --- | --- | --- |
| **Hỗ trợ vận hành** |
| Hỗ trợ kiểm tra thông tin gọi vào Game | Yêu cầu hỗ trợ tra soát log gọi vào game. | Các thông tin cần kiểm tra: url, secretkey, transID.<br> Ví dụ: Nhờ team kiểm tra thông tin gọi API callbackBilling Live của webpayment đã đúng với môi trường live của đối tác hay chưa: http://abcgames.com:12345/callbackBilling|
| Kiểm tra hiện tượng bất thường | Hỗ trợ kiểm tra hiện tượng bất thường liên quan tới API như: API không trả về dữ liệu, API đối tác trả lỗi, API đối tác trả sai thông tin, … | Thông tin cần cho yêu cầu này gồm:<br>- Thông tin chi tiết hiện tượng bất thường.<br>- Hình ảnh đính kèm (nếu có). |
| Hỗ trợ đối soát (User Wallet) | Hỗ trợ các vấn đề liên quan đến đối soát: <br>- Xuất số liệu đối soát báo cáo định kỳ hàng tháng.<br>- Hỗ trợ kiểm tra khi có sai lệch giao dịch. | Thông tin cần cho yêu cầu này gồm:<br>- Mục đích đối soát<br>- Mã giao dịch<br>- UserID, RoleID, ServerID<br>- Thời gian giao dịch<br>Ví dụ: - Mục đích đối soát: Hiện tại phía đối tác đã ghi nhận 2 log giao dịch này, tuy nhiên phía webpay lẫn team payment đều không có log. Nhờ team check thử nguyên nhân do đâu giúp products. <br>- Mã giao dịch: 1670709272578629632, 1670709272578629632<br>- UserID: 1641215881239535616<br>- Thời gian: 24-05-2020 |
| Khác | Các yêu cầu khác về Game API. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. |
| **Tích hợp mới** |
| API giao hàng vào game | Tích hợp các API giao hàng vào Game (IAP, Payment, Promotion). | Thông tin API cần tích hợp: url, secretkey, document của đối tác (trong trường hợp API khác chuẩn của GT)...<br>Ví dụ: Nhờ team change địa chỉ callbackBilling IAP của game thành như sau: <br>Android: https://android.abcgames.123/VietnamVngA <br>IOS: https://ios.abcgames.123/VietnamVngI |
| API cho Web Paymnet | Tích hợp các API cho Web Payment. | Thông tin cần cho yêu cầu này gồm:<br>- Tên API<br>- Thông tin API cần tích hợp: url, secretkey, document của đối tác (trong trường hợp API khác chuẩn của GT)...<br>Danh sách API đang cung cấp cho Web Payment:<br>- API getServers<br>- API getServers by UserID<br>- API getRoles<br>- API getRoles by RoleID<br>- API getRoles by UserID<br>- API getProducts<br>- API getTrans<br>- API callbackBilling<br>Ví dụ:Đối tác bổ sung thông tin thêm API getServers by UserID, nhờ team tích hợp để tối ưu hóa bước chọn server cho user:[_http://abcgames.com/api/](http://abcgames.com/api/getServersByUserID.php) <br>Tài liệu trong file đính kèm. |
| API cho các dịch vụ khác | Cung cấp API liên quan đến game cho các dịch vụ khác. | Thông tin cần cho yêu cầu này gồm:<br>- Tên API<br>- Thông tin API cần tích hợp: url, secretkey, document của đối tác (trong trường hợp API khác chuẩn của GT)...<br>Danh sách các API team đang cung cấp:<br>- API addItems<br>- API setCodes<br>- API getCCU<br>- API getTopRanking<br>- API getPlayerInfo<br>Ví dụ: Nhờ Team cung cấp thông tin API setCodes để phục vụ việc build trang nhập code. |
| Khác | Các yêu cầu khác về Game API. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. |

### **3. Payment – Hệ thống thanh toán, bán hàng.**

Các vấn đề hỗ trợ trong dịch vụ này bao gồm:

| Vấn đề hỗ trợ | Mô tả chi tiết vấn đề hỗ trợ | Thông tin cần người yêu cầu cung cấp |
| --- | --- | --- |
| **Hỗ trợ vận hành** |
| Cấu hình Sandbox Plan Test cho IAP | Hỗ trợ tạo và cập nhật hệ thống IAP Verify để dùng account sandbox test IAP. | Thông tin cần cho yêu cầu này gồm:<br>- Thời gian test.<br>- ServerID đăng ký test.<br>- Lượng giao dịch.<br>Ví dụ:_Game: ZingSpeed Mobile <br>Thời gian test: 18/05/2020 - 29/05/2020 <br>ServerID đăng ký test: 94, 96 <br>Lượng giao dịch: 200 |
| Cấu hình Gói vật phẩm cho Web Payment/IAP | Cập nhật các thông tin liên quan gói vật phẩm bán trên In-app purchase và Web Payment (Cập nhật hình ảnh, thông tin goid, bật/tắt gói theo thời gian, thêm gói mới, cập nhật vị trí hiển thị của gói, …) | Thông tin cần cho yêu cầu này gồm:<br>- File excel thông tin theo format của GT. <br>Click [LINK](https://docs.google.com/spreadsheets/d/15RhF43IiPWfr4SJxT57qyaL-_v3F1R5v/edit#gid=1472447714) để download template.<br>- File hình ảnh của các gói theo chuẩn của GT. <br>Click [LINK](https://drive.google.com/drive/folders/1xRQPxIKh-Dp0GbKPobAO4bfY5cralUcJ) để xem hướng dẫn<br>Ví dụ: Nhờ team cấu hình gói vật phẩm mới (thông tin gói được gửi đính kèm).<br>Thời gian ra lease mong muốn: 18/08/2020 |
| Bổ sung/thay đổi tracking trên Web Payment | Cập nhật/thay đổi thông tin tracking trên Web Payment: Google Analytics, Google Tag Manager, Google Firebase, Appsflyer, Facebook Pixel, …Thông tin chi tiết: [TẠI ĐÂY](https://docs.gt.vng.vn/docs_webpayment/tracking/) | Thông tin cần cho yêu cầu này gồm:<br>GAID, FacebookPixelID, Google Ad Tag... hoặc thông tin cấu hình Firebase.<br>Ví dụ:Google Ad Tag:<br> &lt;script async src=&quot;https://www.googletagmanager.com /gtag/js?id=AW-123456&quot;\&gt;\&lt;/script&gt; <br>&lt;script&gt; window.dataLayer = window.dataLayer \\\\ [];function gtag(){dataLayer.push(arguments);}gtag(&#39;js&#39;, new Date());gtag(&#39;config&#39;, &#39;AW-654321&#39;);&lt;/script&gt; |
| Kiểm tra và Retry giao dịch | Hỗ trợ kiểm tra và Retry giao dịch. | Thông tin cần cho yêu cầu này gồm:Loại giao dịch (IAP Google, IAP Apple, SEA, Việt Nam); User ID, mã giao dịch [GPA], ngày giao dịch.<br>Ví dụ: Nhờ team kiểm tra giao dịch bị báo lỗi sau<br>User ID: 1015059517673701376<br>Mã giao dịch [GPA]: GPA.3301-5677-8879-40210<br>Ngày giao dịch: 15-08-2020 19:37:05<br>Ảnh hóa đơn giao dịch [Tệp đính kèm] |
| Hỗ trợ đối soát | Hỗ trợ các vấn đề liên quan đối soát. | BO/FA cung cấp log, chi tiết thời gian, kênh cần đối soát để hỗ trợ kiểm tra. |
| Kiểm tra thông tin Promotion trên Web Payment | Hỗ trợ cung cấp log giao dịch các vấn đề liên quan promotion như: user không nhận được quà khi nạp, user nhận quà không đúng khi nạp,… | Thông tin cần cho yêu cầu này gồm:<br>- Loại giao dịch<br>- User ID<br>- Mã giao dịch<br>- Ngày giao dịch<br>- Đính kèm hình ảnh chụp màn hình giao dịch. |
| Kiểm tra hiện tượng bất thường | Yêu cầu kiểm tra hệ thống khi có hiện tượng bất thường liên quan đến Payment. | Thông tin cần cho yêu cầu này gồm:<br>- Thông tin chi tiết hiện tượng bất thường liên quan đến Payment<br>- Hình ảnh đính kèm (nếu có) |
| Cấp quyền truy cập tool vận hành | Yêu cầu cấp quyền truy cập vào tool vận hành sản phẩm:<br>- Tool Payment Report.<br>- Tool kiểm tra trạng thái giao dịch IAP/Web Payment<br>- Tool phân tích dữ liệu Web Payment **Yêu cầu này cần sự phê duyệt từ PM của sản phẩm.** | Thông tin cần cho yêu cầu này gồm:<br>- Loại tool cần hỗ trợ truy cập<br>- Tên (domain) user cần cấp quyền truy cập **Yêu cầu sau khi được kiểm tra thông tin sẽ được chuyển cho PM của sản phẩm xét duyệt.**<br>Ví dụ:_Nhờ team hỗ trợ cấp quyền truy cập tool kiểm tra giao dịch cho sản phẩm<br>Các Domain cần cấp quyền: datlc, linhnn6.|
| Hỗ trợ ý tưởng, các vấn đề UI/UX Web Payment | Hỗ trợ ý tưởng, giải quyết các vấn đề về UI/UX Web Payment. |
| Khác | Các yêu cầu khác về Payment. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. |
| **Tích hợp mới** |
| IAP Fulfillment cho Game Mobile | Cung cấp tính năng IAP verify dùng cho các sản phẩm mobile. | Thông tin cần cho yêu cầu này gồm:<br>- Bộ key của sản phẩm được cung cấp bới team PASS-SDK gồm<br>+ GameID<br>+ BundleID<br>+ Package Name<br>+ Public Key<br>- Ngoài ra password của bộ key được cung cấp qua trao đổi riêng<br>- Danh sách item: bổ sung vào yêu cầu sau khi đã có.<br>Ví dụ: _Hiện tại bên team SDK đang chuẩn bị build bộ SDK cho game A ở thị trường Việt Nam sử dụng API Receipt Validation mới cho Payment IAP, nhờ team hỗ trợ cung cập bộ key tích hợp với API này cho game.<br>- Thông tin game:<br>- GameID: TULC<br>- BundleID: com.vng.tuyetung (app real)<br>- Package name (Android): com.vng.tuyetung<br>- GG Public key: xxxx|
| Web Payment cho game | Cung cấp Web Payment cho các sản phẩm mới. | Thông tin cần cho yêu cầu này gồm:<br>- File request webpay<br>- File thông tin hình ảnh webpay<br>- API document<br>- Thời gian release webpay của sản phẩm<br>- Thời gian dự kiến có giấy phép |
| Promotion trên Web Payment | Cung cấp API liên quan đến game cho các dịch vụ khác. | Yêu cầu các chương trình promotion trên Web Payment dành cho các sản phẩm mới.<br>Ví dụ: Nhờ web payment setup giúp promotion quà miễn phí mỗi ngày. <br>Có đính kèm File config.|
| Kênh thanh toán mới | Tích hợp kênh thanh toán mới. |
| Yêu cầu khác | Các yêu cầu khác về tích hợp Payment. Anh/Chị có thể liên hệ TOM để được tư vấn, hỗ trợ thêm. |

### **4. Promotion – Dịch vụ khuyến mãi.**

Các vấn đề hỗ trợ trong dịch vụ này bao gồm:

| Vấn đề hỗ trợ | Mô tả chi tiết vấn đề hỗ trợ | Thông tin cần người yêu cầu cung cấp |
| --- | --- | --- |
| Promotion/Event mới | Yêu cầu Promotion/ Event mới. | Thời gian bắt đầu chương trình. |
| Hỗ trợ | Các yêu cầu hỗ trợ liên quan Promotion. |   |

### **5. Hệ thống khác – Chưa xác định.**

Bao gồm các dịch vụ chưa được liệt kê cụ thể ở các mục trên, hoặc loại
yêu cầu mà người yêu cầu không xác định chính xác được danh mục dịch vụ.
Vui lòng liên hệ TOM để được tư vấn và hỗ trợ thêm cho trường hợp này.

## III. Hướng dẫn thực hiện thao tác tạo yêu cầu trực tiếp.

### **1. Login vào hệ thống.**

Truy cập vào link: <https://gt.vng.vn/ticket> màn hình sau sẽ xuất hiện:

![](../images/gtsupport/image1.png)

User cần đăng nhập vào tài khoản O365, OTP hay RSA trước khi được điều
hướng vào hệ thống GT Support.

![](../images/gtsupport/image2.png)

Sau đó User sẽ thấy màn hình chính của hệ thống GT Support:

![](../images/gtsupport/image3.png)

### **2. Tạo yêu cầu hỗ trợ.**

Tại màn hình chính của hệ thống GT Support.

Nhấp chọn nút **\[Tạo yêu cầu hỗ trợ\]**
![](../images/gtsupport/image4.png), màn hình nhập các thông tin yêu cầu hỗ
trợ như bên dưới xuất hiện:

![](../images/gtsupport/image5.png)

Tại đây người yêu cầu nhập các thông tin cần thiết như sau:

**(1) Sản phẩm cần hỗ trợ**

Chọn sản phẩm muốn yêu cầu bằng 2 cách:

-   Chọn trong danh sách được xổ xuống.

-   Hoặc tìm sản phẩm chính xác bằng cách nhập 1 trong các thông tin: FA
    code, Alias, Tên sản phẩm.

![](../images/gtsupport/image6.png)

(\*) **“Sản phẩm nội bộ”**: Thể hiện danh sách các sản phẩm nội bộ của
GT (Dành riêng cho các yêu cầu xuất phát từ trong nội bộ GT).

**(2) Liên quan đến hệ thống/ Vấn đề hỗ trợ**

Chọn dịch vụ và vấn đề cần được hỗ trợ (tham khảo chi tiết mục I.2. Danh
sách các dịch vụ hỗ trợ)

![](../images/gtsupport/image7.png)

\(a) Dịch vụ cần hỗ trợ

\(b) Vấn đề hỗ trợ

\(c) Mô tả chi tiết vấn đề hỗ trợ, bao gồm: Các nội dung hỗ trợ; thời
gian phản hồi dự kiến; …

**Lưu ý:** Người yêu cầu cần xác định đúng dịch vụ và vấn đề cần hỗ trợ
sẽ giúp yêu cầu được hỗ trợ xử lý nhanh hơn. Nên hạn chế chọn “Hệ thống
khác – Chưa xác định” sẽ mất nhiều thời gian xác minh thông tin và xử lý
vấn đề.

Trường hợp người yêu cầu chưa thể xác định được đúng dịch vụ và vấn đề
cần được hỗ trợ, vui lòng liên hệ nhân sự chính hỗ trợ sản phẩm của phía
GT là TOM phụ trách chính sản phẩm đó.

**(3) Tiêu đề (tóm tắt yêu cầu hỗ trợ)**

Nhập tiêu đề của yêu cầu cần được hỗ trợ.

![](../images/gtsupport/image8.png)

**Lưu ý: **

-   Tiêu đề nên đơn giản, ngắn gọn. Chiều dài tiêu đề giới hạn 150 ký tự
    bao gồm: Thông tin sản phẩm - Dịch vụ hỗ trợ (Liên quan đến hệ
    thống) - Nội dung tiêu đề.

-   Tiêu đề sẽ là Subject của email nhận được từ hệ thống GT Support sau
    khi yêu cầu được khởi tạo hoàn tất.

**(4) Người theo dõi**

Ngoài Product Manager (PM), TOM là 2 đối tượng mặc định sẽ nhận được
thông báo khi có yêu cầu về sản phẩm thì người yêu cầu có thể thêm người
theo dõi cho yêu cầu của mình bằng cách nhập thông tin Domain.

![](../images/gtsupport/image9.png)

**(5) Nội dung yêu cầu hỗ trợ**

Ở mục này người yêu cầu có thể mô tả chi tiết nội dung cần hỗ trợ.

![](../images/gtsupport/image10.png)

Tùy vào loại vấn đề cần hỗ trợ, người yêu cầu cần phải cung cấp thêm một
số thông tin cho yêu cầu của mình. Nội dung cần cung cấp thêm sẽ được
hiển thị ở dạng đề nghị (như hình bên dưới) khi rê chuột vào biểu tượng
![](../images/gtsupport/image11.png)

![](../images/gtsupport/image12.png)

**(6) Đính kèm tệp tin**

Người yêu cầu có thể đính kèm thêm các tệp tin để làm rõ thêm các yêu
cầu của mình. Giới hạn 5 tệp, mỗi tệp có dung lượng không quá 5Mb.

![](../images/gtsupport/image13.png)

**(7) Xác nhận**

Sau khi đã xác nhận các thông tin yêu cầu đã được nhập đầy đủ và đúng,
người yêu cầu nhấp chọn nút **\[Xác nhận\]**
![](../images/gtsupport/image14.png)}để hoàn tất việc tạo yêu cầu hỗ trợ.

### **3. Email thông báo yêu cầu.**

Sau thao tác xác nhận tạo yêu cầu, yêu cầu vừa được tạo sẽ được gán 1 ID
(ex: \#T7559) và được hiển thị trên hệ thống GT Support như sau:

![](../images/gtsupport/image15.png)

Đồng thời với việc hiển thị trên hệ thống GT Support, thông báo về yêu
cầu sẽ được gửi đến email các đối tượng liên quan: Người yêu cầu, người
xử lý, PM, TOM, người theo dõi (nếu có).

![](../images/gtsupport/image16.png)

## IV. Hướng dẫn thực hiện thao tác tạo yêu cầu giúp người khác.

Về cơ bản các bước tạo yêu cầu giúp người khác sẽ tương tự như tự tạo
yêu cầu trực tiếp, chỉ khác ở một số thao tác sau:

-   Người tạo giúp: Đánh dấu là tạo yêu cầu giúp người khác.

-   Người được tạo giúp: Xác nhận lại thông tin đã nhờ người khác tạo
    giúp yêu cầu.

***Ví dụ**: Yêu cầu “PUBGM – Payment – Hỗ trợ - Cấu hình gói vật phẩm
cho Web Payment/ IAP – Hỗ trợ thêm gói …”*

*- Người tạo giúp: Trương Thị Tú Uyên.*

*- Người được tạo giúp: Nguyễn Thị Thu. *

### **1. Tạo yêu cầu giúp người khác.**

Sau khi truy cập hệ thống GT Support, chọn thao tác **\[Tạo yêu cầu hỗ
trợ\]** thì ở phần điền thông tin yêu cầu hỗ trợ ngoài những thông tin
cần điền như tự tạo yêu cầu trực tiếp thì cần lưu ý:

\(a) Đánh dấu chọn mục **“Tạo giúp người khác”**.

\(b) Chọn người được tạo giúp bằng cách nhập thông tin Domain.

**Lưu ý:** Chỉ có thể tạo giúp yêu cầu cho người liên quan tới sản phẩm.

![](../images/gtsupport/image17.png)

Sau khi “người tạo giúp” tạo xong yêu cầu thì yêu cầu vừa được tạo sẽ
được gán 1 ID (ex: \#T7560) và được hiển thị trên hệ thống GT Support
như sau:

![](../images/gtsupport/image18.png)

Trạng thái hiện tại của yêu cầu là: **Chờ xác nhận**.

Người tạo yêu cầu hiện tại là: người tạo giúp (*Trương Thị Tú Uyên*).

Đồng thời lúc này “người được tạo giúp” cũng sẽ nhận được 1 email thông
báo.

### **2. Xác nhận thông tin yêu cầu đã được tạo giúp.**

![](../images/gtsupport/image19.png)

“Người được tạo giúp” nhận được email thông báo có yêu cầu cần được xác
nhận sẽ thông qua ID trong email để vào hệ thống GT Support (hoặc vào
trực tiếp nếu đã từng vào hệ thống) và xác nhận yêu cầu với các bước như
sau:

-   Kiểm tra lại thông tin cần yêu cầu đã được tạo giúp. Ở bước này
    “người được tạo giúp” có thể thêm một số thông tin yêu cầu như: Thêm
    ghi chú, thêm tệp mới, thêm người theo dõi.

-   Nhấn nút **\[Xác nhận thông tin\]**.

![](../images/gtsupport/image20.png)

-   “Người được tạo giúp” có thể xác nhận yêu cầu hoặc hủy yêu cầu.

> ![](../images/gtsupport/image21.png)

-   Nếu chọn **\[Hủy yêu cầu\]** thì “người được tạo giúp” cần đưa ra lý
    do hủy. Và yêu cầu sẽ được chuyển sang trạng thái “**Yêu cầu bị từ
    chối**” và kết thúc tiến trình tại đây.

-   Nếu chọn **\[Xác nhận\]** thì yêu cầu sẽ được chuyển sang trạng thái
    “**Mới tạo**”, tên người tạo sẽ được chuyển sang tên của “người được
    tạo giúp” và tiến trình của yêu cầu sẽ tiếp tục bình thường như yêu
    cầu tự tạo trực tiếp (chi tiết mục IV. Tiến trình của yêu cầu).

![](../images/gtsupport/image22.png)

> Trạng thái của yêu cầu sau khi được xác nhận là: **Mới tạo**.
>
> Người tạo yêu cầu hiện tại là: người được tạo giúp (*Nguyễn Thị Thu*).

## V. Xác nhận yêu cầu.

Với đặc thù phân quyền và chuyển đổi ảnh hưởng trực tiếp đến người sử
dụng, nên các yêu cầu liên quan đến 2 dịch vụ sau đây cần được người
quản lý của dự án (PM) phê duyệt trước khi GT tiếp nhận và xử lý yêu
cầu:

![](../images/gtsupport/image23.png)

Về cơ bản các bước tạo yêu cầu cần xác nhận sẽ tương tự như tự tạo yêu
cầu bình thường, chỉ khác ở một số thao tác sau:

-   Người quản lý cần phê duyệt nội dung của yêu cầu.

-   Người xử lý yêu cầu của GT chỉ tiếp nhận và xử lý yêu cầu sau khi
    yêu cầu được phê duyệt.

Sau khi người yêu cầu hoàn tất việc tạo yêu cầu trên hệ thống GT
Support, nếu xét thấy yêu cầu cần được phê duyệt thì người xử lý yêu cầu
sẽ chuyển trạng thái của yêu cầu sang “**Chờ duyệt**”.

![](../images/gtsupport/image24.png)

Lúc này người quản lý của dự án (PM) sẽ nhận được email yêu cầu xác nhận
nội dung yêu cầu như sau:

![](../images/gtsupport/image25.png)

Người quản lý của dự án (PM) nhận được email thông báo có yêu cầu cần
được xác nhận sẽ thông qua ID trong email để vào hệ thống GT Support
(hoặc vào trực tiếp nếu đã từng vào hệ thống) và duyệt yêu cầu với các
bước như sau:

-   Kiểm tra lại thông tin của yêu cầu. Ở bước này người quản lý dự án
    (PM) có thể thêm một số thông tin yêu cầu như: Thêm ghi chú, thêm
    tệp mới, thêm người theo dõi.

-   Nhấn nút **\[Duyệt yêu cầu\]**.

![](../images/gtsupport/image26.png)

-   Tương tự như các yêu cầu khác, người quản lý dự án (PM) có thể xác
    nhận yêu cầu hoặc hủy yêu cầu.

<!-- -->

-   Nếu chọn **\[Hủy yêu cầu\]** thì người quản lý dự án (PM) cần đưa ra
    lý do hủy. Và yêu cầu sẽ được chuyển sang trạng thái “**Yêu cầu bị
    từ chối**” và kết thúc tiến trình tại đây.

-   Nếu chọn **\[Xác nhận\]** thì yêu cầu sẽ được chuyển sang trạng thái
    “**Đang thực hiện**” và tiến trình của yêu cầu sẽ tiếp tục bình
    thường (chi tiết mục IV. Tiến trình của yêu cầu).

## VI. Tiến trình của yêu cầu.

### **1. Quản lý chung.**

User truy cập link: <https://gt.vng.vn/ticket>

Các yêu cầu liên quan đến user sẽ được hiển thị ở dạng danh sách ở phần
**(a)**, đồng thời user cũng có thể lọc các yêu cầu theo tùy chọn tương
ứng ở bộ lọc dữ liệu phần **(b)**.

![](../images/gtsupport/image27.png)

### **2. Quản lý chi tiết từng yêu cầu.**

Ở màn hình chung, nhấp chọn yêu cầu cần xem chi tiết, màn hình sau sẽ
hiển thị ghi nhận các hoạt động xử lý yêu cầu của các bên liên quan:

![](../images/gtsupport/image28.png)

Các thông tin thể hiện qua lịch sử hoạt động bao gồm:

\(a) Thông tin người thực hiện các hoạt động.

> \(b) Độ ưu tiên của yêu cầu, bao gồm 5 cấp độ từ thấp đến cao: Thấp; Bình
> thường; Cao; Gấp; Cực gấp.
>
> \(c) Các thông tin trao đổi giữa các bên liên quan, user có thể nhập bằng
> cách nhấn vào nút **\[Thêm ghi chú\]**.
>
> \(d) Thông tin thời gian dự kiến thực hiện yêu cầu và trạng thái hiện tại
> của yêu cầu (được chuyển từ Mới tạo sang Đang thực hiện)

\(e) Tiến độ thực hiện yêu cầu, được thể hiện bằng số %.

![](../images/gtsupport/image29.png)

Sau khi yêu cầu được hoàn tất, người yêu cầu cần kiểm tra lại kết quả hỗ
trợ và đóng yêu cầu.

![](../images/gtsupport/image30.png)

*Hình: Yêu cầu đã được xử lý xong và chuyển sang trạng thái “**Đã
xong**”.*

![](../images/gtsupport/image31.png)

> *Hình: Chọn **\[Đóng yêu cầu\]** để đóng yêu cầu đã được xử lý.*

![](../images/gtsupport/image32.png)

> *Hình: Yêu cầu được chuyển sang trạng thái “**Đóng yêu cầu**”.*

## VII. Các tiện ích hỗ trợ.

### **1. Tutorial**

Đối với user lần đầu tiên truy cập hệ thống GT Support, các thao tác
thực hiện trên hệ thống sẽ được hướng dẫn tự động thông qua tutorial.
User có thể thực hiện theo từng bước được hướng dẫn, hoặc bỏ qua nếu
muốn.

![](../images/gtsupport/image33.png){

Muốn mở lại các hướng dẫn sau khi đã đóng, user chọn vào biểu tượng bóng
đèn ![](../images/gtsupport/image34.png)ở các
góc màn hình.

### **2. Phản hồi**

User có thể đưa ra các góp ý để GT cải thiện hệ thống GT Support bằng
cách nhấp chọn nút **\[Phản hồi\]** nhấp nháy ở góc trái màn hình.

![](../images/gtsupport/image35.png)
